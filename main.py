# -*- coding: utf-8 -*-
"""
Created on Wed Sep 30 13:29:41 2020

@author: nrvdw
"""
import numpy as np
np.seterr(all='ignore')
np.seterr(divide = 'ignore')
import ISMRM_Phantom_Tools as ipt
import constants as C
import matplotlib.pyplot as plt
from tqdm import tqdm
# import math
import xlsxwriter
from datetime import date
import os


##########################################

wb = xlsxwriter.Workbook(os.path.join(C.output_path, 'Results_' + str(date.today()) + '.xlsx'))
used_data = wb.add_worksheet('Used_data')
not_used_sheet = wb.add_worksheet('Unused_data')
row_not_used = 0
row_used = 1
not_used_dir = []

if __name__ == '__main__':
    dcm_path_list = ipt.dcm_list_builder(C.root_path)     

    for idx in tqdm(range(len(dcm_path_list))):
        dcm_name = dcm_path_list[idx]
        # print(dcm_name)
            
        try:
        # if True:
            header, dcm_array = ipt.dcm_reader(dcm_path_list[idx])
            
            top_phantom = ipt.find_top_phantomV2(dcm_array, header)
                        
            background_mean = np.mean(dcm_array[:,:,top_phantom - 2])
            
            mean_phantom, sd_phantom = ipt.find_mean_phantom(dcm_array, top_phantom,\
                                                  background_mean, header)
            
            planes,  obj_coor_T1, obj_coor_T2, obj_coor_PD = ipt.find_planes_spheres(dcm_array.copy(), header, mean_phantom,\
                                      top_phantom, plot_planes = True, plot_centers = True)
                
                
            plane_key = 'T1'
            planes[plane_key] = ipt.calc_mean_SD_object(planes[plane_key], plane_key, obj_coor_T1,\
                                              dcm_array[:,:,planes[plane_key][0]].copy(), header, plot_ROI = True)
            
            plane_key = 'T2'
            planes[plane_key] = ipt.calc_mean_SD_object(planes[plane_key], plane_key, obj_coor_T2,\
                                              dcm_array[:,:,planes[plane_key][0]].copy(), header, plot_ROI = False)
                
            plane_key = 'PD'
            planes[plane_key] = ipt.calc_mean_SD_object(planes[plane_key], plane_key, obj_coor_PD,\
                                              dcm_array[:,:,planes[plane_key][0]].copy(), header, plot_ROI = False)
    
            # fiducials = ipt.fiducial_sphere_finder(planes['T1'][0],dcm_array, header,\
            #                                           mean_phantom, plot_fiducials = True)
                
            # geometry_distortion = ipt.fiducial_dist_calc(fiducials, header)
                
            # MTF, MTF_0_5, ESF, LSF = ipt.calc_MTF_ISMRM(dcm_array[:,:,planes['T2'][0]],\
            #                                     mean_phantom[planes['T2'][0]], header, plot_MTF = True)
            
            
            row_used = ipt.write_ISMRM_to_excel(used_data, row_used, dcm_name, planes, header, mean_phantom, sd_phantom)
        
        except:
            not_used_sheet.write(row_not_used, 0, dcm_name)
            not_used_dir.append(dcm_name)
            row_not_used += 1
        
wb.close()
