# -*- coding: utf-8 -*-
"""
Created on Wed Sep 30 13:29:57 2020

@author: nrvdw
"""


###############################################################################
#### Imports
###############################################################################
import numpy as np
#np.seterr(all='ignore')
import os, os.path
import pydicom
import matplotlib.pyplot as plt
import cv2
import scipy.signal
import scipy
import operator
import math
import statistics
from skimage.transform import rotate
from scipy import interpolate
from scipy import optimize


###############################################################################
#### Functions
###############################################################################
def dcm_list_builder(path, test_text = ""):
    # function to get list of dcm_files from dcm directory
    dcm_path_list = []
    for (dirpath, dirnames, filenames) in os.walk(path,topdown=True):
        if dirpath not in dcm_path_list:
            for filename in filenames:
                try:
                    tmp_str = str('\\\\?\\' + os.path.join(dirpath, filename))
                    pydicom.read_file(tmp_str, stop_before_pixels = True)
                    if dirpath not in dcm_path_list:
                        dcm_path_list.append(dirpath)
                except:
                    pass
            else:
                pass
    return dcm_path_list


def dcm_reader(dcm_path):
    #read dcm_files from path
    #path certainly contains DCM files, as tested by dcm_list_builder function
    dcm_files = []
    for (dirpath, dirnames, filenames) in os.walk(dcm_path,topdown=False):
        for filename in filenames:
            try:
                dcm_file = str('\\\\?\\' + os.path.join(dirpath, filename))
                pydicom.read_file(dcm_file, stop_before_pixels = True)
                dcm_files.append(dcm_file)
            except:
                pass

    read_RefDs = True
    while read_RefDs:
        for index in range(len(dcm_files)):
            try:
                RefDs = pydicom.read_file(dcm_files[index], stop_before_pixels = False)
                read_RefDs = False
                break
            except:
                pass
            
    ConstPixelDims = (int(RefDs.Rows), int(RefDs.Columns), len(dcm_files))
    dcm_array = np.zeros([ConstPixelDims[0],ConstPixelDims[1],len(dcm_files)],\
                          dtype=RefDs.pixel_array.dtype) 

    
    instances = []    
    for filenameDCM in dcm_files:
        try:
            ds = pydicom.read_file(filenameDCM, stop_before_pixels = True)
            instances.append(int(ds.InstanceNumber))
        except:
            pass
    
    instances.sort()
    
    index = 0
    for filenameDCM in dcm_files:
        try:
            ds = pydicom.read_file(filenameDCM)
            dcm_array[:,:,instances.index(ds.InstanceNumber)] = ds.pixel_array
            if ds.InstanceNumber in instances[:2]:
                if ds.InstanceNumber == instances[0]:
                    loc_1 = ds.SliceLocation
                else:
                    loc_2 = ds.SliceLocation
            index += 1
        except:
            pass
        
    try:
        RefDs.SliceThickness = abs(loc_1 - loc_2)
    except:
        pass
    
    dcm_array = np.flip(dcm_array, 2)

    return RefDs, dcm_array


def dcm_threshold(array, threshold, multiplier = 1): 
    if len(array.shape) == 2:
        tmp_array = array
        tmp_array[tmp_array <= threshold * multiplier] = 0
        tmp_array[tmp_array >= threshold * multiplier] = 1
            
        
    else:
        for index in range(array.shape[2]):
            tmp_array = array[:,:,index]
            tmp_threshold = threshold[index] * multiplier
     
            tmp_array[tmp_array <= tmp_threshold] = 0
            tmp_array[tmp_array >= tmp_threshold] = 1
     
            array[:,:,index] = tmp_array
        
    array = array.astype(dtype = np.uint8)
 
    return array


def dcm_filtering(array, image_kernel = 3):
    if image_kernel % 2 == 0:
        image_kernel += 1
        
    if len(array.shape) == 2:
        array_filtered = scipy.signal.medfilt2d(array, image_kernel)
    else:
        array_filtered = np.zeros_like(array)
        for index_slice in range(array.shape[2]):
            array_filtered[:,:,index_slice] = scipy.signal.medfilt2d(array[:,:,index_slice], image_kernel)
    
    return array_filtered


def find_top_phantom(array, header):
    mean_mask_dict = {}
    for index in range(2,array.shape[2]):
        center_arr = [int(header.Columns / 2), int(header.Rows / 2)]
        size_roi = int(30 / header.PixelSpacing[0]**2)
        
        array_reduced = array[center_arr[0] - size_roi:center_arr[0] + size_roi,\
                              center_arr[1] - size_roi:center_arr[1] + size_roi,\
                                  index]
            
        mean_mask_dict[index] = array_reduced.mean()
        # plt.imshow(array_reduced)
        # plt.show()
        
    first_nonzero_mean = 0
    for index in range(2, int(array.shape[2] / 2)):
        if mean_mask_dict[index] == 0:
            pass
        else:
            first_nonzero_mean = index
            break
    
    small_index = array.shape[2] + 1
    for index in range(first_nonzero_mean + 1, int(array.shape[2] / 2)):
        test_diff = mean_mask_dict[index] / mean_mask_dict[first_nonzero_mean]
        print(index, test_diff)
        if test_diff > 3:
            if index < small_index:
                small_index = index
                
    top_phantom = small_index
        
    return top_phantom


def find_top_phantomV2(array, header):
    radius_mask = 70 / header.PixelSpacing[0]
    size_mask = math.pi * radius_mask **2
    mask = create_circular_mask(array.shape[0], array.shape[1],\
                                [int(array.shape[0]/2), int(array.shape[1]/2)],\
                                    radius_mask)
        
    size_ROI_BG = int(30 / header.PixelSpacing[0])
    size_ROI_center = int(20 / header.PixelSpacing[0])
    
    exp_size = math.pi * (50 / header.PixelSpacing[0])**2
    
    top_indices = []
    for index in range(2,int(array.shape[2] / 2)):
        mean_background = array[0:size_ROI_BG, 0:size_ROI_BG, index].mean()
        mean_background = array[int(20 / header.PixelSpacing[0]):\
                                int(20 / header.PixelSpacing[0]) + 2*size_ROI_BG,\
                                int(array.shape[1]/2) - size_ROI_BG:\
                                int(array.shape[1]/2) + size_ROI_BG, index].mean()
        mean_center = array[int(array.shape[0]/2) - size_ROI_center:\
                            int(array.shape[0]/2) + size_ROI_center,\
                            int(array.shape[1]/2) - size_ROI_center:\
                            int(array.shape[1]/2) + size_ROI_center, index].mean()
        
        if mean_center > 0:
            if mean_center > 3 * mean_background:
                thres = (mean_center + mean_background) * 0.5
            
                plane_array = array[:,:,index].copy()
                plane_array[plane_array < thres] = 0
                plane_array = plane_array * mask
                plane_array[plane_array > 0] = 1
                plane_array = plane_array.astype(dtype = np.uint8)
                
                # plt.imshow(plane_array)
                # plt.title(index)
                # plt.show()
                
                output = cv2.connectedComponentsWithStats(plane_array, 4, cv2.CV_32S)
                if output[0] > 1:
                    if (output[2][1][4] > 0.90 * exp_size and output[2][1][4] < 0.9 * size_mask):
                        # print(index, mean_background, mean_center, thres, output[2][1][4], exp_size)
                        top_indices.append(index)
                       
    top_phantom = min(top_indices)
        
    return top_phantom



def create_circular_mask(h, w, center_circle, radius_circle):

    Y, X = np.ogrid[:h, :w]
    dist_from_center = np.sqrt((X - center_circle[1])**2 + (Y-center_circle[0])**2)

    mask = dist_from_center <= radius_circle
    
    return mask


def find_mean_phantom(array, top_index, background, header):
    mean_phantom = {}
    sd_phantom = {}
    
    bottom_phantom = top_index + math.ceil(180 / header.SliceThickness)
    center = [int(header.Columns / 2), int(header.Rows / 2)]
    start = int(int(header.Rows / 2) - (28 / header.PixelSpacing[0]))
    end = int(start + (16 / header.PixelSpacing[0]))
    box_width = int(3 / header.PixelSpacing[0])
   
    for index in range(top_index, bottom_phantom):
        array_box = array[start:end,center[1] - box_width:center[1] + box_width,index].copy()
        mean_phantom[index] = array_box.mean()
        sd_phantom[index] = array_box.std()
        
        # vizualization of mean calc:
        # array[start:end,center[1] - box_width:center[1] + box_width,index] = 1000
        # plt.imshow(array[:,:,index])
        # plt.show()

    for index in range(array.shape[2]):
        if index not in mean_phantom:
            mean_phantom[index] = background
            sd_phantom[index] = 0
        elif mean_phantom[index] == math.inf:
            mean_phantom[index] = background
            sd_phantom[index] = 0
        elif math.isnan(mean_phantom[index]):
            mean_phantom[index] = background
            sd_phantom[index] = 0
    
    mean_phantom_ordered = {}
    sd_phantom_ordered = {}
    for index in range(array.shape[2]):
        mean_phantom_ordered[index] = mean_phantom[index]
        sd_phantom_ordered[index] = sd_phantom[index]
    
    return mean_phantom_ordered, sd_phantom_ordered


def find_spheres(tmp_array, header):
    tmp_array = scipy.signal.medfilt2d(tmp_array, 3)

    output = cv2.connectedComponentsWithStats(tmp_array, 4, cv2.CV_32S)
    
    centers = []
    count_spheres = 0
    for index2 in range(1,output[0]):
        physical_size = (math.pi * (16/2)**2) / header.PixelSpacing[0]**2
        measured_size = output[2][index2][4]
        if measured_size in range (int(physical_size * 0.6), int(physical_size * 1.5)):
            start_coordinate = [int(output[3][index2][1]), int(output[3][index2][0])]
            
            x_right = 0
            while tmp_array[start_coordinate[0], start_coordinate[1] + x_right] == 1:
                x_right += 1
            
            x_left = 0
            while tmp_array[start_coordinate[0], start_coordinate[1] - x_left] == 1:
                x_left += 1
            
            y_top = 0
            while tmp_array[start_coordinate[0] + y_top, start_coordinate[1]] == 1:
                y_top += 1
            
            y_bottom = 0
            while tmp_array[start_coordinate[0] - y_bottom, start_coordinate[1]] == 1:
                y_bottom += 1
                
            x_dist = x_right + x_left
            y_dist = y_top + y_bottom
            
            if x_dist in range(int(0.7*y_dist), int(1.2*y_dist)):
                count_spheres += 1
                centers.append([round(output[3][index2][1]), round(output[3][index2][0])])
                
    return count_spheres, centers


def distance_between_points(coor1, coor2):
    x1 = coor1[0]
    x2 = coor2[0]
    y1 = coor1[1]
    y2 = coor2[1]
    
    distance = math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
    
    return distance
   

def update_center(arr, coor_old, header, mean_plane):
    size_obj = math.ceil(18 / header.PixelSpacing[0])
    
    max_range = 0.75
    count_down = size_obj + 1
    count_up = size_obj + 1
    count_right = size_obj + 1
    count_left = size_obj + 1
    try:        
        while (count_down >= size_obj or\
               count_up >= size_obj or\
               count_right >= size_obj or\
               count_left >= size_obj):
            range_plane_max = max_range * mean_plane
            count_down = 0
            
            down = []
            for index in range(size_obj):
                down.append(arr[coor_old[0] + index, coor_old[1]])
            
            for element in down:
                if element < range_plane_max:
                    break
                else:
                    count_down += 1    
            
            up = []
            for index in range(size_obj):
                up.append(arr[coor_old[0] - index, coor_old[1]])
            count_up = 0
            for element in up:
                if element < range_plane_max:
                    break
                else:
                    count_up += 1    
            
            right = []
            for index in range(size_obj):
                right.append(arr[coor_old[0], coor_old[1] + index])
            count_right = 0
            for element in right:
                if element < range_plane_max:
                    break
                else:
                    count_right += 1
                    
            left = []
            for index in range(size_obj):
                left.append(arr[coor_old[0], coor_old[1] - index])
            count_left = 0
            for element in left:
                if element < range_plane_max:
                    break
                else:
                    count_left += 1
                
            # print(range_plane_max)
            # print(down)
            # print(up)
            # print(right)
            # print(left)
            
            # print(count_down, count_up, count_right, count_left)
            
            max_range += 0.05
            
        for element in down:
            if down[count_down + 1] < down[count_down]:
                count_down += 1
            else:
                break
        for element in up:
            if up[count_up + 1] < up[count_up]:
                count_up += 1
            else:
                break
        for element in right:
            if right[count_right + 1] < right[count_right]:
                count_right += 1
            else:
                break
        for element in left:
            if left[count_left + 1] < left[count_left]:
                count_left += 1
            else:
                break
            
        
            
        # print(count_down, count_up, count_right, count_left)
            
        point_1 = [coor_old[0] + count_down, coor_old[1]]
        point_2 = [coor_old[0] - count_up, coor_old[1]]
        point_3 = [coor_old[0], coor_old[1] + count_right]
        point_4 = [coor_old[0], coor_old[1] - count_left] 
            
        center_obj = [point_1, point_2, point_3, point_4]
        x,y = zip(*center_obj)
        coor_new = [round((max(x)+min(x))/2.), round((max(y)+min(y))/2)]

    except:
        coor_new = coor_old
    
    return coor_new    
    
    

def find_obj_coor(arr_plane, center_four, header, mean_plane):
    x,y = zip(*center_four)
    center = (max(x)+min(x))/2., (max(y)+min(y))/2
    
    coor = {}
    
    # find coordinates of objects
    # first inner objects
    # start counting clockwise, from upper right object = 1
    coor['inner_1'] = points_closest(center_four, [0,header.Rows], 1)[0]
    coor['inner_2'] = points_closest(center_four, [header.Columns, header.Rows], 1)[0]
    coor['inner_3'] = points_closest(center_four, [header.Columns,0], 1)[0]
    coor['inner_4'] = points_closest(center_four, [0,0], 1)[0]
    
    angle_inner_1 = math.asin((coor['inner_1'][1] - center[1])/\
                              distance_between_points(coor['inner_1'],center)) + 0.5 * math.pi
    angle_inner_2 = math.asin((coor['inner_2'][1] - center[1])/\
                              distance_between_points(coor['inner_2'],center))
    angle_inner_3 = math.asin((coor['inner_3'][1] - center[1])/\
                              distance_between_points(coor['inner_3'],center)) 
    angle_inner_4 = math.asin((coor['inner_4'][1] - center[1])/\
                              distance_between_points(coor['inner_4'],center)) - 0.5 * math.pi
        
    # print(angle_inner_1, angle_inner_2, angle_inner_3, angle_inner_4)
        
        
    radius = 51 / header.PixelSpacing[0]
    arc_obj = 2 * math.pi / 10
    #calc outer object centers for position 1
    angle_1 = 8    
    angle_outer_1 = angle_inner_1 + (2 * math.pi * angle_1 /  360) + arc_obj
    angle_outer_2 = angle_inner_1 + (2 * math.pi * angle_1 /  360)
    angle_outer_3 = angle_inner_1 + (2 * math.pi * angle_1 /  360) - arc_obj
    
    #calc outer object centers for position 2    
    angle_2 = 27 
    angle_outer_4 = angle_inner_2 + (2 * math.pi * angle_2 /  360)
    angle_outer_5 = angle_inner_2 + (2 * math.pi * angle_2 /  360) - arc_obj
    
    #calc outer object centers for position 3    
    angle_3 = 8
    angle_outer_6 = angle_inner_3 + (2 * math.pi * angle_3 /  360) + arc_obj
    angle_outer_7 = angle_inner_3 + (2 * math.pi * angle_3 /  360)
    angle_outer_8 = angle_inner_3 + (2 * math.pi * angle_3 /  360) - arc_obj
    
    #calc outer object centers for position 4    
    angle_4 = 10 
    angle_outer_9 = angle_inner_4 - (2 * math.pi * angle_4 /  360) + arc_obj
    angle_outer_10 = angle_inner_4 - (2 * math.pi * angle_4 /  360)
    
    
    angles = [angle_outer_1, angle_outer_2, angle_outer_3,\
              angle_outer_4, angle_outer_5, angle_outer_6,\
              angle_outer_7, angle_outer_8, angle_outer_9, angle_outer_10]
    
        
    for index in range(len(angles)):
        coor['outer_' + str(index + 1)] = [round(center[0] + (radius * math.cos(angles[index]))),\
                                          round(center[1] + (radius * math.sin(angles[index])))]

    for index in range(len(angles)):
        # if index == 8:
        coor_ind = 'outer_' + str(index + 1)
        coor[coor_ind] = update_center(arr_plane, coor[coor_ind], header, mean_plane)  

    return coor


def find_planes_spheres(array, header, mean_phantom, top_phantom, plot_planes = False, plot_centers = False):
    ori = array.copy()
    mask = create_circular_mask(array.shape[0], array.shape[1],\
                                [int(array.shape[0]/2), int(array.shape[1]/2)],\
                                    (80 / header.PixelSpacing[0])/2)
    
    for index in range(array.shape[2]):
        array[:,:,index][array[:,:,index] < mean_phantom[index] * 1.2] = 0
        array[:,:,index] = array[:,:,index] * mask

    array[array > 0] = 1
    array = array.astype(dtype = np.uint8)
        
    bottom_phantom = top_phantom + math.ceil(180 / header.SliceThickness)
    if bottom_phantom > array.shape[2]:
        bottom_phantom = array.shape[2]
    else:
        pass
    
    T1_slices = []
    for index in reversed(range(top_phantom + int(130 / header.SliceThickness), bottom_phantom - int(20 / header.SliceThickness))):
        tmp_array = array[:,:,index]
        # plt.imshow(tmp_array)
        # plt.show()

        count_spheres, _ = find_spheres(tmp_array, header)

        if count_spheres == 4:
            T1_slices.append(index)
    
    planes = {}
    planes['T1'] = [math.ceil(statistics.median(T1_slices))]
    planes['T2'] = [planes['T1'][0] - math.ceil(40 / header.SliceThickness)]
    planes['PD'] = [planes['T1'][0] - math.ceil(80 / header.SliceThickness)]
    
    for key in planes.keys():
        plane_array_ori = ori[:,:,planes[key][0]].copy()
        mean_plane = mean_phantom[planes[key][0]]
        # plane_array_ori = scipy.signal.medfilt2d(plane_array_ori, 3)
        
        thres = 1.3
        num_spheres_found = 0
        while num_spheres_found != 4:
            plane_array = plane_array_ori.copy()
            plane_array[plane_array < mean_phantom[planes[key][0]] * thres] = 0
            plane_array = plane_array * mask
            plane_array[plane_array > 0] = 1
            plane_array = plane_array.astype(dtype = np.uint8)
            # plt.imshow(plane_array)
            # plt.show()
        
            _, center_four = find_spheres(plane_array, header)
            
            # print(center_four)
        
            num_spheres_found = len(center_four)
            thres -= 0.1

        if key == 'T1':
            obj_coor_T1 = find_obj_coor(plane_array_ori, center_four, header, mean_plane)
        elif key == 'T2':
            obj_coor_T2 = find_obj_coor(plane_array_ori, center_four, header, mean_plane)
        elif key == 'PD':
            obj_coor_PD = obj_coor_T2
        
    
    if plot_planes:
        plt.subplot(131)
        plt.title('T1')
        plt.imshow(ori[:,:,planes['T1'][0]])
        plt.subplot(132)
        plt.title('T2')
        plt.imshow(ori[:,:,planes['T2'][0]])
        plt.subplot(133)
        plt.title('PD')
        plt.imshow(ori[:,:,planes['PD'][0]])
        plt.show()
        
    if plot_centers:
        T1_plane = ori[:,:,planes['T1'][0]].copy()
        vizu = T1_plane.max() * 2
        for key in obj_coor_T1:
            T1_plane[obj_coor_T1[key][0], obj_coor_T1[key][1]] = vizu
        
        T2_plane = ori[:,:,planes['T2'][0]].copy()
        vizu = T2_plane.max() * 2
        for key in obj_coor_T2:
            T2_plane[obj_coor_T2[key][0], obj_coor_T2[key][1]] = vizu
        
        PD_plane = ori[:,:,planes['PD'][0]].copy()
        vizu = PD_plane.max() * 2
        for key in obj_coor_PD:
            PD_plane[obj_coor_PD[key][0], obj_coor_PD[key][1]] = vizu

        plt.title('T1')
        plt.imshow(T1_plane)
        plt.show()

        plt.title('T2')
        plt.imshow(T2_plane)
        plt.show()
        
        plt.title('PD')
        plt.imshow(PD_plane)
        plt.show()
        
    return planes, obj_coor_T1, obj_coor_T2, obj_coor_PD



def calc_mean_SD_object(plane, plane_ID, obj_centers, arr, header, plot_ROI = False):
    IQ_plot = arr.copy()
    IQ_plot_max = IQ_plot.max() * 2
    
    for key in obj_centers.keys():
        #Create circular mask based on center of sphere, for IQ measurements
        IQ_mask = create_circular_mask(header.Rows, header.Columns,\
                                        [obj_centers[key][0], obj_centers[key][1]],\
                                        round(5 / header.PixelSpacing[0]))
            
        IQ_array = IQ_mask * arr
        
        mean_value = IQ_array[np.where(IQ_mask == 1)].mean()
        SD_value = IQ_array[np.where(IQ_mask == 1)].std()
        
        plane.append([round(mean_value,2), round(SD_value,2)])
        
        IQ_plot[np.where(IQ_mask == 1)] = IQ_plot_max
                
    if plot_ROI:
        plt.imshow(IQ_plot)
        plt.title(plane_ID)
        plt.show()        
        
    return plane

def calc_dist(coord1, coord2):
    if len(coord1) == 2:
        dist = math.sqrt((coord1[0] - coord2[0])**2 + (coord1[1] - coord2[1])**2)
    elif len(coord1) == 3:
        dist = math.sqrt((coord1[0] - coord2[0])**2 + (coord1[1] - coord2[1])**2 + (coord1[2] - coord2[2])**2)
    else:
        print('Error, wrong size of vector for distance calculation')
    
    return dist


def points_closest(points, test_coordinate, K):
 
    points.sort(key = lambda K: (K[0] - test_coordinate[0])**2 + (K[1] - test_coordinate[1])**2)
 
    return points[:K]
 


def fiducial_dist_calc(fiducials, header):
    # measurements are performed for 3 planes
    # 1. Coronal
    # 2. Sagital
    # 3. Axial
    # for each plane, measurements are clockwise
    # measurement 1 means from 1-o'clock position, connected to 7 o'clock position
    
    distortion = {}
    
    # first, calculate coordinates from coronal plane
    top_coordinates = points_closest(fiducials['middle'][1], [0,int(header.Rows/2)], 3)
    eleven_oclock = points_closest(top_coordinates, [0,0], 1)[0]
    #twelve_oclock = points_closest(top_coordinates, [0,int(header.Rows/2)], 1)[0]
    one_oclock = points_closest(top_coordinates, [0,int(header.Rows)], 1)[0]
    
    bottom_coordinates = points_closest(fiducials['middle'][1], [int(header.Columns),int(header.Rows/2)], 3)
    five_oclock = points_closest(bottom_coordinates, [int(header.Columns),int(header.Rows)], 1)[0]
    #six_oclock = points_closest(bottom_coordinates, [int(header.Columns),int(header.Rows/2)], 1)[0]
    seven_oclock = points_closest(bottom_coordinates, [int(header.Columns),0], 1)[0]
    
    right_coordinates = points_closest(fiducials['middle'][1], [int(header.Columns/2),int(header.Rows)], 3)
    two_oclock = points_closest(right_coordinates, [0,int(header.Rows)], 1)[0]
    #three_oclock = points_closest(right_coordinates, [int(header.Columns/2),int(header.Rows)], 1)[0]
    four_oclock = points_closest(right_coordinates, [int(header.Columns),int(header.Rows)], 1)[0]
    
    left_coordinates = points_closest(fiducials['middle'][1], [int(header.Columns/2),0], 3)
    eight_oclock = points_closest(left_coordinates, [int(header.Columns),0], 1)[0]
    #nine_oclock = points_closest(left_coordinates, [int(header.Columns/2),0], 1)[0]
    ten_oclock = points_closest(left_coordinates, [0,0], 1)[0]
    
    # Second, calculate coordinates for top_bottom measurements
    bottom_12_oclock = points_closest(fiducials['bottom'][1],\
                                      [0,int(header.Rows/2)], 1)[0]+ [fiducials['bottom'][0]]
    bottom_3_oclock = points_closest(fiducials['bottom'][1],\
                                     [int(header.Columns/2),int(header.Rows)], 1)[0]+ [fiducials['bottom'][0]]
    bottom_6_oclock = points_closest(fiducials['bottom'][1],\
                                     [int(header.Columns),int(header.Rows/2)], 1)[0]+ [fiducials['bottom'][0]]
    bottom_9_oclock = points_closest(fiducials['bottom'][1],\
                                     [int(header.Columns/2),0], 1)[0]+ [fiducials['bottom'][0]]
    
    top_12_oclock = points_closest(fiducials['top'][1],\
                                   [0,int(header.Rows/2)], 1)[0]+ [fiducials['top'][0]]
    top_3_oclock = points_closest(fiducials['top'][1],\
                                  [int(header.Columns/2),int(header.Rows)], 1)[0]+ [fiducials['top'][0]]
    top_6_oclock = points_closest(fiducials['top'][1],\
                                  [int(header.Columns),int(header.Rows/2)], 1)[0]+ [fiducials['top'][0]]
    top_9_oclock = points_closest(fiducials['top'][1],\
                                  [int(header.Columns/2),0], 1)[0]+ [fiducials['top'][0]]
        
    # Third, calculate coordinates for sagital measurements
    sag_high_12_oclock = points_closest(fiducials['axial2'][1],\
                                      [0,int(header.Rows/2)], 1)[0]+ [fiducials['axial2'][0]]
    sag_high_3_oclock = points_closest(fiducials['axial2'][1],\
                                      [int(header.Columns/2),int(header.Rows)], 1)[0]+ [fiducials['axial2'][0]]
    sag_high_6_oclock = points_closest(fiducials['axial2'][1],\
                                      [int(header.Columns),int(header.Rows/2)], 1)[0]+ [fiducials['axial2'][0]]
    sag_high_9_oclock = points_closest(fiducials['axial2'][1],\
                                      [int(header.Columns/2),0], 1)[0]+ [fiducials['axial2'][0]]
        
    sag_low_12_oclock = points_closest(fiducials['axial1'][1],\
                                      [0,int(header.Rows/2)], 1)[0]+ [fiducials['axial1'][0]]
    sag_low_3_oclock = points_closest(fiducials['axial1'][1],\
                                      [int(header.Columns/2),int(header.Rows)], 1)[0]+ [fiducials['axial1'][0]]
    sag_low_6_oclock = points_closest(fiducials['axial1'][1],\
                                      [int(header.Columns),int(header.Rows/2)], 1)[0]+ [fiducials['axial1'][0]]
    sag_low_9_oclock = points_closest(fiducials['axial1'][1],\
                                      [int(header.Columns/2),0], 1)[0]+ [fiducials['axial1'][0]]
        
    
    distortion['cor_1'] = calc_dist(one_oclock, seven_oclock)
    distortion['cor_2'] = calc_dist(two_oclock, eight_oclock)
    distortion['cor_4'] = calc_dist(four_oclock, ten_oclock)
    distortion['cor_5'] = calc_dist(five_oclock, eleven_oclock)
    
    distortion['top_bot_3'] = calc_dist(top_3_oclock, bottom_9_oclock)
    distortion['top_bot_6'] = calc_dist(top_6_oclock, bottom_12_oclock)
    distortion['top_bot_9'] = calc_dist(top_9_oclock, bottom_3_oclock)
    distortion['top_bot_12'] = calc_dist(top_12_oclock, bottom_6_oclock)
    
    distortion['sag_high_low_3'] = calc_dist(sag_high_3_oclock, sag_low_9_oclock)
    distortion['sag_high_low_6'] = calc_dist(sag_high_6_oclock, sag_low_12_oclock)
    distortion['sag_high_low_9'] = calc_dist(sag_high_9_oclock, sag_low_3_oclock)
    distortion['sag_high_low_12'] = calc_dist(sag_high_12_oclock, sag_low_6_oclock)
    
    return distortion



def find_fiducial_loc(sphere_centers, coord):
    dist_coord = {}
    for element in sphere_centers:
        dist_tmp = calc_dist(element, coord)
        dist_coord[sphere_centers.index(element)] = dist_tmp
        
    min_dist = min(dist_coord.items(), key=operator.itemgetter(1))[0]
    
    return min_dist


def fiducial_center_finder(array, slice_num, mean_phantom, header):
    array = array[:,:,slice_num].copy()
        
    array = dcm_threshold(array, mean_phantom[slice_num], multiplier = 1.5)
    array_filtered = dcm_filtering(array, image_kernel = 3)
   
    plot_image_fiducials = array_filtered.copy()
    
    # plt.imshow(plot_image_fiducials)
    # plt.show()
    
    output = cv2.connectedComponentsWithStats(array_filtered, 4,cv2.CV_32S)
    
    fiducial_centers = {}
    #add all centers if larger in range of physical area of spheres
    for index in range(1,output[0]):
        physical_size = math.pi * (5 / header.PixelSpacing[0]) ** 2
        measured_size = output[2][index][4]
        if measured_size in range (4, int(physical_size * 1.5)):
            fiducial_centers[index] = [int(output[3][index][1]), int(output[3][index][0])]
            
    poppable_keys = []
    for key2 in fiducial_centers.keys():
        start_coordinate = [fiducial_centers[key2][0], fiducial_centers[key2][1]]
        
        x_right = 0
        while array_filtered[start_coordinate[0], start_coordinate[1] + x_right] == 1:
            x_right += 1
        
        x_left = 0
        while array_filtered[start_coordinate[0], start_coordinate[1] - x_left] == 1:
            x_left += 1
        
        y_top = 0
        while array_filtered[start_coordinate[0] + y_top, start_coordinate[1]] == 1:
            y_top += 1
        
        y_bottom = 0
        while array_filtered[start_coordinate[0] - y_bottom, start_coordinate[1]] == 1:
            y_bottom += 1
            
        x_dist = x_right + x_left
        y_dist = y_top + y_bottom
        
        if x_dist not in range(int(0.5*y_dist), int(1.5*y_dist)):
            poppable_keys.append(key2)
        else:
            pass
        
    for key3 in poppable_keys:
            fiducial_centers.pop(key3)
    
    centers = []        
    for key4 in fiducial_centers.keys():
        centers.append(fiducial_centers[key4])
    
    
    return centers, plot_image_fiducials


def fiducial_plane_estimator(array, slice_min, slice_max, mean_value, expected, header):
    array = dcm_threshold(array, mean_value, multiplier = 1.5)
    
    array_filtered = dcm_filtering(array[:,:,slice_min - 1:slice_max], image_kernel = 5)
    
    slice_num_circles = {}
    for slice_index in range(array_filtered.shape[2]):
        # plt.imshow(array_filtered[:,:,slice_index])
        # plt.show()
        
        output = cv2.connectedComponentsWithStats(array_filtered[:,:,slice_index], 4,cv2.CV_32S)
    
        num_circles = 0
        physical_size = math.pi * (5 / header.PixelSpacing[0]) ** 2
        
        for index in range(1,output[0]):
            measured_size = output[2][index][4]
            if measured_size in range (3, int(physical_size * 1.3)):
                num_circles += 1
        
        slice_num_circles[slice_index] = num_circles

    max_found = max(slice_num_circles.values())
    max_keys = []
    for key in slice_num_circles.keys():
        if slice_num_circles[key] == max_found:
            max_keys.append(key)
            
    slice_fiducial = int(statistics.median(max_keys))
    
    return slice_fiducial


def expected_fiducials(key):
    if key == 'top':
        expected = 5
    elif key == 'axial1':
        expected = 13
    elif key == 'middle':
        expected = 21
    elif key == 'axial2':
        expected = 13
    elif key == 'bottom':
        expected = 4
    return expected


def fiducial_sphere_finder(T1_plane, array, header, mean_phantom, plot_fiducials = False):
    fiducials = {}
    fiducials['top'] = [T1_plane - int(132 / header.SliceThickness)]
    fiducials['axial1'] = [T1_plane - int(92 / header.SliceThickness)]
    fiducials['middle'] = [T1_plane - int(52 / header.SliceThickness)]
    fiducials['axial2'] = [T1_plane - int(12 / header.SliceThickness)]
    fiducials['bottom'] = [T1_plane + int(28 / header.SliceThickness)]
    
    for key in fiducials.keys():
        expected = expected_fiducials(key)

        slice_min = fiducials[key][0] - int(3 / header.SliceThickness)
        slice_max = fiducials[key][0] + int(3 / header.SliceThickness)

        slice_updater = fiducial_plane_estimator(array.copy(), slice_min, slice_max,\
                                                 mean_phantom, expected, header)
        
        fiducials[key] = [fiducials[key][0] - int(5 / header.SliceThickness) + slice_updater]

    for key in fiducials.keys():
        centers, fiducial_image = fiducial_center_finder(array, fiducials[key][0], mean_phantom, header)
        
        if len(centers) != expected_fiducials(key):
            centers, fiducial_image = fiducial_center_finder(array, fiducials[key][0] + 1, mean_phantom, header)
            if len(centers) != expected_fiducials(key):
                centers, fiducial_image = fiducial_center_finder(array, fiducials[key][0] - 1, mean_phantom, header)
                if len(centers) != expected_fiducials(key):
                    print('\nNumber of fiducials in plane', key, '-', fiducials[key][0],
                          '- not as expected. Assess influence on geometric distortion.\n')
                    
        fiducials[key].append(centers) 
        if plot_fiducials:
            plt.imshow(fiducial_image)
            plt.show()

    return fiducials


def center_updater(array_binary, mean_phantom):
    array_binary[array_binary < mean_phantom / 3] = 0
    array_binary[array_binary > 0] = 1
    array_binary = array_binary.astype(dtype = np.uint8)  
    
    output = cv2.connectedComponentsWithStats(array_binary, 4, cv2.CV_32S)

    sizes = []
    for index in range(1,output[0]):
        sizes.append(output[2][index][4])
        
    center_index = sizes.index(max(sizes)) + 1

    center_updated = [int(output[3][center_index][1]), int(output[3][center_index][0])]
    
    return center_updated


def ESF_fitter(ESF):
    def Fermi_func(x, d, a1, a2, a3, b1, b2, b3, c1, c2, c3):
        return d + (a1 / (np.exp((x - b1)/c1) + 1)) +\
            (a2 / (np.exp((x - b2)/c2) + 1)) +\
                (a3 / (np.exp((x - b3)/c3) + 1))

    A = 0.005
    B = 0.025
    R = 9.5
    d = A
    a1 = 0.75*(B-A)
    a2 = 0.125*(B-A)
    a3 = 0.125*(B-A)
    b1 = R
    b2 = R - 0.5
    b3 = R - 0.5
    c1 = 0.15
    c2 = 0.14
    c3 = 0.14
    
    pixels = np.arange(0,len(ESF))
    pixels_new = np.arange(0, len(ESF), 0.01)
    
    params, params_covariance = optimize.curve_fit(Fermi_func, pixels, ESF,
                                                p0=[d, a1, a2, a3, b1,\
                                                    b2, b3, c1, c2, c3],\
                                                    method = 'lm', maxfev = 50000)
    
    # plt.figure(figsize=(6, 4))
    # plt.scatter(pixels, ESF, label='Data')
    # plt.plot(pixels, Fermi_func(pixels, params[0], params[1],\
    #                         params[2], params[3], params[4],\
    #                         params[5], params[6],\
    #                         params[7], params[8], params[9]\
    #                             ), label='Fitted function')
    
    # plt.legend(loc='best')
    # plt.show()
    
    ESF = []
    for element in pixels_new:
        ESF.append(Fermi_func(element, params[0], params[1],\
                            params[2], params[3], params[4],\
                            params[5], params[6],\
                            params[7], params[8], params[9]))
    
    return ESF


def ESF_logistic(ESF):
    def Fermi_func(x, d, a1, a2, a3, b1, b2, b3, c1, c2, c3):
        return d + (a1 / (np.exp((x - b1)/c1) + 1)) +\
            (a2 / (np.exp((x - b2)/c2) + 1)) +\
                (a3 / (np.exp((x - b3)/c3) + 1))

    A = 0.005
    B = 0.025
    R = 9.5
    d = A
    a1 = 0.75*(B-A)
    a2 = 0.125*(B-A)
    a3 = 0.125*(B-A)
    b1 = R
    b2 = R - 0.5
    b3 = R - 0.5
    c1 = 0.15
    c2 = 0.14
    c3 = 0.14
    
    pixels = np.arange(0,len(ESF))
    pixels_new = np.arange(0, len(ESF), 0.01)
    
    params, params_covariance = optimize.curve_fit(Fermi_func, pixels, ESF,
                                                p0=[d, a1, a2, a3, b1,\
                                                    b2, b3, c1, c2, c3],\
                                                    method = 'lm', maxfev = 50000)
    
    # plt.figure(figsize=(6, 4))
    # plt.scatter(pixels, ESF, label='Data')
    # plt.plot(pixels, Fermi_func(pixels, params[0], params[1],\
    #                         params[2], params[3], params[4],\
    #                         params[5], params[6],\
    #                         params[7], params[8], params[9]\
    #                             ), label='Fitted function')
    
    # plt.legend(loc='best')
    # plt.show()
    
    ESF = []
    for element in pixels_new:
        ESF.append(Fermi_func(element, params[0], params[1],\
                            params[2], params[3], params[4],\
                            params[5], params[6],\
                            params[7], params[8], params[9]))
    
    return ESF


def calc_MTF_ISMRM(array, mean_phantom, header, plot_MTF = False):
    center_updated = center_updater(array.copy(), mean_phantom)
    
    angles = list(range(360))
    array_MTF = array.copy()
    
    ESF = [0]
    for index in angles:
        tmp_array = array_MTF.copy()
        tmp_array = rotate(tmp_array,index)
        new_ESF = tmp_array[center_updated[0],\
                            center_updated[1] + int(90/header.PixelSpacing[0]):
                            center_updated[1] + int(110/header.PixelSpacing[0])]

        if len(ESF) == 1:
            ESF = new_ESF
        else:
            ESF += new_ESF
    ESF = ESF / len(angles)
    
    #ESF = ESF_fitter(ESF)
    ESF = ESF_logistic(ESF)
    
    LSF = np.abs(np.diff(ESF))
    LSF = LSF*np.hanning(LSF.shape[0])
    MTF = np.abs(np.fft.fft(LSF)) / sum(LSF)
    MTF_x = np.linspace(0,len(LSF), len(LSF)) / (len(LSF))
    MTF_y = MTF[:int(len(LSF)/2)]
    MTF_x = MTF_x[:int(len(LSF)/2)]
    MTF = [MTF_x, MTF_y]

    if plot_MTF:
        plt.plot(MTF[0], MTF[1])
        #plt.xlim(0,0.005)
        #plt.ylim(0,1)
        plt.show()
        
    MTF_0_5 = np.interp(0.5, np.flip(MTF_y,0), np.flip(MTF_x,0))
    return MTF, MTF_0_5, ESF, LSF


def write_ISMRM_to_excel(used_data, row_used, dcm_name, planes, header, mean, SD):
    column = 0
    scan_name = dcm_name.rsplit('\\')[len(dcm_name.rsplit('\\'))-1]
    
    obj_ID = [0, 'inner_1', 'inner_2', 'inner_3', 'inner_4', 'outer_1',\
                  'outer_2', 'outer_3', 'outer_4', 'outer_5', 'outer_6',\
                  'outer_7', 'outer_8', 'outer_9', 'outer_10']
    
    used_data.write(0, 0, 'Phantom')
    used_data.write(0, 1, 'Scan_description')
    used_data.write(0, 2, 'Plane')
    used_data.write(0, 3, 'Sphere_ID')
    used_data.write(0, 4, 'Mean')
    used_data.write(0, 5, 'SD')
    used_data.write(0, 6, 'CNR')
    
   
    for key in planes.keys():
        for index in range(1, len(planes[key])):
            used_data.write(row_used, column, 'ISMRM')
            used_data.write(row_used, column + 1, scan_name)
            used_data.write(row_used, column + 2, key)
            used_data.write(row_used, column + 3, obj_ID[index])
            used_data.write(row_used, column + 4, str(planes[key][index][0]))
            used_data.write(row_used, column + 5, str(planes[key][index][1]))
            CNR = abs(mean[planes[key][0]] - planes[key][index][0]) / SD[planes[key][0]]
            used_data.write(row_used, column + 6, str(round(CNR,1)))
            
            row_used += 1

    return row_used 